Overview
--------

This module provides a tip jar block to request donations and
describe the amount of recent contributions.  It relies on
the paypal_framework.module to receive IPN information from
PayPal.  If PHP was compiled with PNG support in the GD
library, a small image can be generated to graphically
depict the amount of recent contributions.

Installation
------------

1) Install paypal_framework.module.
2) Copy paypal_tipjar.module to the modules/ directory.
3) Go to admin/settings/paypal_tipjar to configure it.

README.txt version
------------------
